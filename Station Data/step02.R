#Author: Dr. Remegio Confesor
#####    Interactive README1st   ######
cat(paste("\n"))
cat(paste("This code rearranges the data structure of the station into columns: \n")) 
cat(paste("  Two columns for each parameter (reporting code): Date and value. \n")) 
cat(paste("    The R objects and files are saved as 'M_oepano_StationNo.ed'  \n")) 
cat(paste("\n"))
cat(paste("\n"))
cat(paste("Calculating... this will take a while! \n"))
img.files <- list.files(pattern = "\\.img")
for (numfile in 1:length(img.files)) {
temp.file <- img.files[numfile]
# temp.file <- img.files[177]
temp.data <- get(load(temp.file))
#nchar(temp.file) get the length of the string
len.file <- nchar(temp.file)
temp.name01 <- substr(temp.file,8,len.file-4)
n.stations <- as.array(attributes(split(temp.data,temp.data$station))$names) # check for number of stations

for (nstat in 1:length(n.stations)) {

temp.name02 <- paste("M_",temp.name01,"_",n.stations[nstat],sep="")
temp.data02 <- assign(temp.name02,subset(temp.data,station==n.stations[nstat])) #needed only for n.stations >1
rep.codes <- as.array(attributes(split(temp.data02,temp.data02$reporting_code))$names)
rep.codes.name <-  paste("rc",rep.codes,sep="")
temp.name03 <- paste(temp.name02,".ed",sep="")
#assign(temp.name03,matrix(NA,nrow=length(temp.data02[,1]),ncol=2*length(rep.codes)))
#temp01 <- get(temp.name03)
count = 0
#temp01 <- matrix(NA,nrow=length(temp.data02[,1]),ncol=2*length(rep.codes))
temp01 <- matrix(NA,nrow=6941,ncol=2*length(rep.codes))
eve.num <- seq(2,2*length(rep.codes),2)
odd.num <- seq(1,2*length(rep.codes),2)
coln.temp <- matrix(NA,nrow=1,ncol=length(rep.codes)*2)
coln.temp[odd.num] <- paste("D",rep.codes,sep="")
coln.temp[eve.num] <- rep.codes.name
colnames(temp01) <- coln.temp
for (i in 1:length(rep.codes)){
assign(paste(rep.codes.name[i]),subset(temp.data02,temp.data02$reporting_code==rep.codes[i]))
temp02 <- get(rep.codes.name[i])
    for (j in 1:length(temp01[,1])) {
        temp01[j,1+count] <- as.integer(temp02[j,3])  
        temp01[j,2+count] <- temp02[j,5]  
} # for (j in 1:length(temp01[,1]))
        count = count + 2
} # for (i in 1:length(rep.codes))
   img.tempfile <- paste(temp.name03,".img",sep="")
   csv.tempfile <-  paste(temp.name03,".csv",sep="")
   assign(temp.name03,temp01)
   save(list=temp.name03,file=img.tempfile)
   write.table(temp01,file=csv.tempfile,sep=",",col.names=T,row.names=F)

}# for (nstat in 1:length(n.stations)) 
} #for (numfiles in 1:length(img.files)) 
cat(paste("\n"))
cat(paste("\n"))
cat(paste("Data rearranged and saved... \n")) 
cat(paste("\n"))
cat(paste("You can now proceed to step 3... \n")) 

