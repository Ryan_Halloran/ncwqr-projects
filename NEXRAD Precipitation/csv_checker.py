########################################
# Author: Ryan Halloran
#
# A short script to assist in checking the work done with "nexrad_02.R".
# Examines a given day's directory, checking for any inconsistancies between 
# the day's "... rate.data.csv" file and the individual timestamp .asc files.
# If a discrepency is discovered, the script produces a file named ERRORLOG.txt
# in the directory where the error occurs.
#
########################################

########## Import Statements ##########

import subprocess
import os

########## Begin Function Definitions ##########

def get_file_list(instruction_string):
	file_list = os.popen(instruction_string).read()
	file_list = file_list.split()

	return file_list

def prepare_timestamp(timestamp_csv):
	timestamp_file = open(timestamp_csv)

	temp_list = []
	for line in timestamp_file:
		temp_list.append(line.strip())

	timestamp_file.close()

	temp_list = temp_list[6::] #skip first six lines

	#turn into 2d list
	for index in range(0, len(temp_list)):
		temp_list[index] = temp_list[index].split(" ")

	#copy target day into 1d list
	single_list = []
	for row in range(0, len(temp_list)):
		for col in range(0, len(temp_list)):
			single_list.append(temp_list[row][col])

	for item in range(0, len(single_list)):
		if single_list[item] == "-999.0000000":
			single_list[item] = "-999"
		elif single_list[item] == "0.0000000":
			single_list[item] = "0"
		elif single_list[item] == "0.1000000":
			single_list[item] = "0.1"
		elif single_list[item] == "0.2500000":
			single_list[item] = "0.25"
		elif single_list[item] == "0.5000000":
			single_list[item] = "0.5"
		elif single_list[item] == "0.7500000":
			single_list[item] = "0.75"
		elif single_list[item] == "1.0000000":
			single_list[item] = "1"
		elif single_list[item] == "1.2500000":
			single_list[item] = "1.25"
		elif single_list[item] == "1.5000000":
			single_list[item] = "1.5"
		elif single_list[item] == "1.7500000":
			single_list[item] = "1.75"
		elif single_list[item] == "2.0000000":
			single_list[item] = "2"
		elif single_list[item] == "2.2500000":
			single_list[item] = "2.25"
		elif single_list[item] == "2.5000000":
			single_list[item] = "2.5"
		elif single_list[item] == "2.7500000":
			single_list[item] = "2.75"
		elif single_list[item] == "3.0000000":
			single_list[item] = "3"

	return tuple(single_list)


def prepare_daily_csv_2d(dailyfilename):
	#open _rate.data.csv file
	dailyfile = open(dailyfilename)	
	
	#convert to list
	templist = []
	for line in dailyfile:
		templist.append(line.strip().split(","))

	#close file handle
	dailyfile.close()

	#remove first row
	templist = templist[1::]
	
	#remove first two columns
	for row in templist:
		del row[0:2]

	#return 2d list as tuple
	return tuple(templist)

def prepare_daily_csv_1d(datinput, datindex):
	
	single_list = []
	for row in range(0, len(datinput)):
		single_list.append(datinput[row][datindex]) 

	return tuple(single_list)


def main(wd, fn):
	working_directory = wd
	os.chdir(working_directory)

	timestamp_files = get_file_list("ls KCLE*.asc")

	dailyfilename = fn
	daily_data_2d = prepare_daily_csv_2d(dailyfilename)

	for index, each_file in enumerate(timestamp_files):
		daily_data_1d = prepare_daily_csv_1d(daily_data_2d, index)
		timestamp = prepare_timestamp(each_file)
		if daily_data_1d != timestamp:
			print("DISAGREEMENT IN " + wd)
			errorlog = open("ERRORLOG.txt","w")
			errorlog.write("OH NO! There was an inconsistancy in the file at " + each_file)
			errorlog.write("\n")
			for each_item in daily_data_1d:
				errorlog.write(each_item)
				errorlog.write(" ")
			errorlog.write("\n")
			errorlog.write(str(len(daily_data_1d)))
			errorlog.write("\n")
			for every_item in timestamp:
				errorlog.write(every_item)
				errorlog.write(" ")
			errorlog.write("\n")
			errorlog.write(str(len(timestamp)))
			errorlog.write("\n")
			errorlog.close()
			print(timestamp)
			return False

	return True

########## End Function Definitions ##########

for i in ("2009", "2010", "2011", "2012", "2013", "2014"):

	print("Now processing water year " + i)

	for j in range(1, 365):
		try:
			working_directory = "/media/SDB1NTFS/NEXRAD-III_WY{0}/WY{1}-Day{2:0>3}/".format(i, i, j)
			water_day = "day_{0:0>3}_rate.data.csv".format(j)
			main(working_directory, water_day)
		except:
			print("ERROR in " + working_directory + ". Likely missing data.")

working_directory = "/media/SDB1NTFS/NEXRAD-III_WY{0}/WY{1}-Day{2:0>3}/".format("2012", "2012", 366)
water_day = "day_{0:0>3}_rate.data.csv".format(366)
main(working_directory, water_day)
