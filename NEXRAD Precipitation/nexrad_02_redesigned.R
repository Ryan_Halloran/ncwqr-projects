################################
# Author: Ryan Halloran
# Based on "nexrad_02_original.R", by Dr. Remegio Confesor and Matt Tossin
#
#
#
################################

#####    Import / Setup    #####

library(parallel)
CORE_NUM <- detectCores()

library(compiler)
enableJIT(3)

##### Function Definitions #####

##### Support Functions    #####

Columnize <- function(files, coords, gridsize=50) {
  #Takes a list of .asc files containing rainfall data and a matrix
  #of coordinates, and returns a matrix containing the coordinates and the
  #data from each .asc file. 
	opened_files <- lapply(files, read.table, skip=6)

  all_data <- matrix(NA, nrow=(gridsize ^ 2), ncol=(length(opened_files) + 2))
  all_data[,1:2] <- coords #appends new_coords to the first two cols

  for (i in 1:length(files)){
    all_data[,(i+2)] <- as.vector(t(opened_files[[i]])) #t() causes row-wise
  }
	
  return(all_data)
}

CreateHeader <- function(files) {
	#Create timestamps for the column headers
	col.headers <- matrix(NA, nrow=1, ncol=length(files))

	for (i in 1:length(files)) {
		temp.time <- substr(files[[i]],28,31)
		t.hour <- as.integer(substr(temp.time, 1, 2))

		t.min <- as.integer(substr(temp.time, 3, 4))

		col.headers[i] <- sprintf("%04d",t.hour * 60 + t.min)
	}

	return(col.headers)
}

NinesToNA <- function(data_set) {
  #Takes the data set and returns it with all instances of -999 and -99
  #converted to NAs. Intended for a matrix, but could be applied broadly.
  data_set[data_set == -999] <- NA
  data_set[data_set == -99] <- NA
  
  return(data_set)
}

GetTimeDifferences <- function(today, yesterday){
  #Takes the column names of *_rate.data,
  #returns the time differences from each column timestamp to the next
  today <- colnames(today)
  yesterday <- colnames(yesterday)

  newvec1 <- today[3:(length(today) - 1)]
  newvec2 <- today[4:length(today)]

  FirstColDiff <- as.numeric(today[3]) + 
                  (1440 - as.numeric(yesterday[length(yesterday)]))
                  #1440 being the number of minutes in a day
  AllOtherDiffs <- as.numeric(newvec2) - as.numeric(newvec1)

  return(append(FirstColDiff, AllOtherDiffs))
}

ApplyTimeDifferences <- function(input_data, diff_vector){
  #Performs row-wise multiplication, applying the time differences
  #of each column to the data in order. Could be de-factorized if need be.

  input_data[,-(1:2)] <- sweep(input_data[,-(1:2)], MARGIN=2, diff_vector, '*')
  return(input_data)
}

OtherArithmetic <- function(input_data){
  #Divides by 60 to convert from hourly to minutely rate,
  #then multiplies by 25.4 to convert from inches to mm.

  input_data[,-(1:2)] <- (input_data[,-(1:2)] / 60) * 25.4

  return(input_data)
}

InitialRowSums <- function(input_data){
  #Sums the daily rainfall data for each location, then appends these as a 
  #new column, named "DayTot".

  thesums <- rowSums(input_data[,-(1:2)], na.rm=TRUE, dims=1)
  input_data <- cbind(input_data, thesums)
  colnames(input_data)[ncol(input_data)] <- 'DayTot'

  return(input_data)
}

RGCalc <- function(yesterday, today){
  #Because the calls to as.numeric coerce the non-numeric colnames into NAs,
  #this function ignores all columns that are not timestamped--including the
  #daily totals.

  suppressWarnings(yest_cols <- as.numeric(colnames(yesterday)))
  suppressWarnings(tod_cols <- as.numeric(colnames(today)))

  yest_cols <- yest_cols >= 480 #turns into logical vector
  tod_cols <- tod_cols < 480

  tmp_matrix <- cbind(yesterday[,yest_cols], today[,tod_cols])

  RG_tot_vec <- rowSums(tmp_matrix, na.rm=TRUE, dims=1)

  today <- cbind(today, RG_tot_vec)
  colnames(today)[ncol(today)] <- 'RGEgTot'
  
  return(today)
}

NAToDoubleNine <- function(data_set){
  #Converts all instances of -99 in the data set to NAs, then returns the data.

  data_set[is.na(data_set)] <- -99

  return(data_set)
}

##### High-Level Functions #####

ProcessDay_One <- function(day_folder, coordinates, gridsize) {

  setwd(day_folder)

  asc_files <- list.files(pattern = "\\.(asc|ASC)$")

  stage_1_file <- Columnize(asc_files, coordinates, gridsize)

  column_headers <- CreateHeader(asc_files)
  colnames(stage_1_file) <- c("lon", "lat", column_headers)

  stage_1_file <- NinesToNA(stage_1_file)

  saveRDS(stage_1_file, file="stage_1_data.rds")

  stage_1_file_path <- paste(day_folder, "/stage_1_data.rds", sep="")
  return(stage_1_file_path)
}

ProcessDay_Two <- function(index, rds_filepaths, day_folders) {

  today <- readRDS(rds_filepaths[[index]])
  yesterday <- readRDS(rds_filepaths[[index-1]])

  difference_vector <- GetTimeDifferences(today, yesterday)
  stage_2_data <- ApplyTimeDifferences(today, difference_vector)

  stage_2_data <- OtherArithmetic(stage_2_data)

  stage_2_data <- InitialRowSums(stage_2_data)

  stage_2_file_path <- paste(day_folders[[index]], "/stage_2_data.rds", sep="")

  saveRDS(stage_2_data, file=stage_2_file_path)

  return(stage_2_file_path)
}

ProcessDay_Three <- function(index, rds_filepaths, day_folders) {

  today <- readRDS(rds_filepaths[[index-1]])
  yesterday <- readRDS(rds_filepaths[[index-2]])

  stage_3_data <- RGCalc(yesterday, today)

  stage_3_data <- NAToDoubleNine(stage_3_data)

  stage_3_rds_file_path <- paste(day_folders[[index]], "/stage_3_data.rds", sep="")
  stage_3_csv_file_path <- paste(day_folders[[index]], "/stage_3_data.csv", sep="")    

  saveRDS(stage_3_data, file=stage_3_rds_file_path)
  write.table(stage_3_data, file=stage_3_csv_file_path, sep=",",
              col.names=T, row.names=F)

  return(stage_3_rds_file_path)
}

#####     Main Function    #####

main <- function(coord_location, gridsize=50, data_folders_file) {

  original_dir <- getwd()

  coordinate_file <- readRDS(coord_location)

  day_folders <- read.table(file=data_folders_file, header=FALSE,
                            comment.char="", colClasses="character")

  day_folders <- as.list(day_folders[,1])


  stage_1_rds_filepaths <- mclapply(day_folders, ProcessDay_One,
                                    coordinates=coordinate_file,
                                    gridsize=gridsize, mc.cores=CORE_NUM)

  setwd(original_dir)

  stage_2_rds_filepaths <- mclapply(2:length(stage_1_rds_filepaths),
                                    ProcessDay_Two,
                                    rds_filepaths=stage_1_rds_filepaths,
                                    day_folders=day_folders, mc.cores=CORE_NUM)

  final_file_locations <- mclapply(3:length(stage_1_rds_filepaths),
                                   ProcessDay_Three,
                                   rds_filepaths=stage_2_rds_filepaths,
                                   day_folders=day_folders, mc.cores=CORE_NUM)

  saveRDS(final_file_locations, file="list_of_stage_3_files.rds")
}

##### End of Function Defs #####

main(coord_location="",
     data_folders_file="")
