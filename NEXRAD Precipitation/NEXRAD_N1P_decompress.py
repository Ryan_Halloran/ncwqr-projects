##############################
#Written for Python 2.6+
#
#A small script which creates a number of subdirectories in a
#format specific to NCWQR projects. NEXRAD data is then decompressed from
#tar.Z or tar.gz files and added to those subdirectories.
#
#
#Author: Ryan Halloran
##############################

import subprocess
import os

def get_file_list():
	file_list = os.popen("ls *.tar*").read()
	file_list = file_list.split()

	return file_list

def wy_days_mkdir(water_year=None, days=None):
	if water_year == None:
		water_year = raw_input("Please input the four-digit water year: ")
	if days == None:
		days = raw_input("Please input the number of days in the water year: ")
		days = int(days)

	for i in range(1, days + 1):
		folder_name = "WY{0}-Day{1:0>3}".format(water_year, i)
		subprocess.call(["mkdir", folder_name])

def make_data_subdirectories(file_list_name=None):
	if file_list_name == None:
		file_list_name = raw_input("Please input the file name: ")
		file_list_name = open(file_list_name, "r")

	for line in file_list_name:
		subdir_name = line[0:29] #copies only up to the date in the file name
		subprocess.call(["mkdir", subdir_name])

	try:
		file_list_name.close()
	except:
		pass

def decompress_nexrad_data(file_list=None):
	if file_list == None:
		file_list = raw_input("Please input the file name: ")
		file_list = open(file_list, "r")

	for line in file_list:
		print("Unpacking {0}".format(line))
		print("to subdir {0}".format(line[0:29]))
		subprocess.call(["tar", "-xvzf", line, "-C", line[0:29], "--wildcards", "--no-anchored", '*N1P*'])
	
	try:
		file_list.close()
	except:
		pass

def main():
	working_directory = raw_input("Please input the working directory: ")
	os.chdir(working_directory)

	file_list = get_file_list()

	wy_days_mkdir()
	make_data_subdirectories(file_list_name=file_list)
	decompress_nexrad_data(file_list=file_list)

if __name__ == "__main__":
	main()
