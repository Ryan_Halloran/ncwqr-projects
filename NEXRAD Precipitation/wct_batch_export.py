##############################
# Author: Ryan Halloran
#
# Written for Python 2.6+
#
# A small script automates the batch export process for precipitation data,
# using the Weather and Climate Toolkit from the NOAA.
#
##############################

######## Import Statements ##########

import subprocess
import os

####### Constants ###################

BATCH_CONFIG = '/home/ryan/Downloads/wct-3.7.6/wctBatchConfig_50.xml'
WCT_EXPORT_SHELL = '/home/ryan/Downloads/wct-3.7.6/wct-export'

#### Functions ####

def open_filelists():
	###

	in_dirs_fileloc = raw_input("Please input the filename listing the" +
														   	" input directories.")
	in_dirs = open(in_dirs_fileloc, "r")
	
	temp_list = []
	for line in in_dirs:
		temp_list.append(line.strip())
	in_dirs.close()
	in_dirs = tuple(temp_list)

	###

	out_dirs_fileloc = raw_input("Please input the filename listing the" + 
															 " output directories.")
	out_dirs = open(out_dirs_fileloc, "r")

	temp_list = []
	for line in out_dirs:
		temp_list.append(line.strip())
	out_dirs.close()
	out_dirs = tuple(temp_list)

	return (in_dirs, out_dirs)
###################################

def main(in_dirs = None, out_dirs = None):
	"""
	Add docstring here. Note: if passed in, in_dirs and out_dirs must be
	lists or tuples. Otherwise, the open_filelists function will be called
  to get them in tuple-ized form. WCT_EXPORT_SHELL and BATCH_CONFIG are
  constants, set at the top of the program, and currently must be edited
  there if a change is desired.
	"""

	while in_dirs == None or out_dirs == None: #or len(in_dirs) != len(out_dirs):
		in_dirs, out_dirs = open_filelists()

	for i in range(0, len(in_dirs)):
		in_string = in_dirs[i]
		out_string = out_dirs[i]

		subprocess.call(["sh", WCT_EXPORT_SHELL, in_string, out_string, "asc", BATCH_CONFIG])

	return True
###################################

main()
